from flask import Flask, Response, request
from flask_cors import CORS
import json
app = Flask(__name__)
CORS(app)

@app.route('/tododata')
def todo_data():
    return json.load(open('todoData.json'))


if __name__ == '__main__':
    # when dockerizing this, uncomment the following line
    app.run(port=8080, debug = True, threaded=True, host=('0.0.0.0'))

    # when dockerizing this, comment the following line
    # app.run(port=8080, threaded=True)